const { Builder, By } = require('selenium-webdriver');
const driver = new Builder().forBrowser('chrome').build();

async function performBrowserActions() {
  try {
    await driver.get('http://127.0.0.1:5500/index.html');
    await driver.findElement(By.id('myButton')).click();

    await driver.findElement(By.name('username')).sendKeys('John Doe');

    await driver.sleep(1000);

    const dropdownElement = await driver.findElement(By.id('dropdownId'));
    await dropdownElement.click();

    const optionElement = await dropdownElement.findElement(By.xpath('//option[text()="2"]'));
    await optionElement.click();

    await driver.sleep(2000);

  } catch (error) {
    console.error('Failed to perform browser actions:', error);
  } finally {
    await driver.quit();
  }
}

performBrowserActions();